import React, { Component } from 'react';
import axios from 'axios';

export default class CreateOfCategory extends Component{
    constructor(props){
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            
        }
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        })
    }
   
    onSubmit(e) {
        e.preventDefault();
        const bodyOptions = {
            name : this.state.name
        }
            const reqOpt = {};
        axios.post('/api/v1/categories/new', bodyOptions, reqOpt)
            .then(res => this.props.history.push("/categories/" + res.data.id))
            .catch(err => this.props.history.push("/error404"));
    }

    render() {
        return (
            <div>
                <h3>Create Category</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName} />
                            
                    </div>
                    
                    <div className="form-group">
                        <input type="submit" value="Create Category" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}