import React, { Component } from 'react';
import axios from 'axios';
import {AuthContext} from '../services/auth';

export default class UpdateBot extends Component{
    constructor(props){
        super(props);

        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeAvaUrl = this.onChangeAvaUrl.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            username: '',
            email: '',
            avaUrl: '',
            
        }
    }

    static contextType = AuthContext;

    componentDidMount(){
        let user = this.context;
        console.log(user);
        if(user.currentUser.id != this.props.match.params.user_id && !user.currentUser.isAdmin){
            this.props.history.push("/error403");
        }
        axios.get('/api/v1/users/' + this.props.match.params.user_id)
        .then(res => res.data)
        .then(user => {
            this.setState({
                username : user.username,
                email : user.email,
                avaUrl : user.avaUrl
            })
        })
        .catch(err => this.props.history.push("/error404"));
    }

    onChangeUsername(e) {
        console.log("here");
        this.setState({
            username: e.target.value
        })
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    onChangeAvaUrl(e) {
        this.setState({
            avaUrl: e.target.files[0]
        })
    }

   
    onSubmit(e) {
        e.preventDefault();

        const fd = new FormData();

            if (this.state.avaUrl)
                fd.append('image', this.state.avaUrl, this.state.avaUrl.name)

            fd.append('email', this.state.email)
            fd.append('username', this.state.username)
            console.log(this.state.avaUrl);
            const reqOpt = {};
        axios.put(`/api/v1/users/${this.props.match.params.user_id}/update`, fd, reqOpt)
            .then(res => this.props.history.push(`/users/${this.props.match.params.user_id}`))
            .catch(err => this.props.history.push("/error404"));
    }

    render() {
        return (
            <div>
                <h3>Update User</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Username: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.username}
                            onChange={this.onChangeUsername} />
                            
                    </div>
                    <div className="form-group">
                        <label>E-mail: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.email}
                            onChange={this.onChangeEmail}
                        />
                    </div>
                    <div className="form-group">
                        <label>Avatar: </label>
                        <input
                            type="file"
                            name="avaUrl"
                            onChange={this.onChangeAvaUrl}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Update User" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
