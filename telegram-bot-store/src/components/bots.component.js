import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Bot = props => (
  <tr>
    <td><Link to={`/bots/${props.bot.id}`}>{props.bot.name}</Link></td>
    <td>{props.bot.price}</td>
  </tr>
)

export default class Bots extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bots: [],
      currentPage: 1,
      botsPerPage: 4
    }

  }

  componentDidMount() {
    const fetchBots = async () => {
      const res = await axios.get('/api/v1/bots');
      this.setState({ bots: res.data });
    };

    fetchBots();
  }

  slicing() {
    const indexOfLastBot = this.state.currentPage * this.state.botsPerPage;
    const indexOfFirstBot = indexOfLastBot - this.state.botsPerPage;

    return this.state.bots.slice(indexOfFirstBot, indexOfLastBot);
  }



  Pagination(botsPerPage, totalBots) {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalBots / botsPerPage); i++) {
      pageNumbers.push(i);
    }

    const changeNumber = (pageNumber) => { this.setState({ currentPage: pageNumber }) }

    return (
      <nav>
        <ul className='pagination'>
          {pageNumbers.map(number => (
            number !== this.state.currentPage ?
            <li key={number} className='page-item'>
              <button onClick={() => {
                changeNumber(number);
                this.slicing();
              }} className='page-link'>
                {number}
              </button>
            </li>
            :
            <li key={number} className='page-item active'>
              <button onClick={() => {
                changeNumber(number);
                this.slicing();
              }} className='page-link'>
                {number}
              </button>
              </li>
          ))}
        </ul>
      </nav>
    );
  };


  render() {
    return (
      <div>
        {

          <div>
            <h3>Bots</h3>
            <table className="table">
              <thead className="thead-light">
                <tr>
                  <th>Bot name</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>

                {
                  Object.values(this.slicing()).map((bot, index) => {
                    return (
                      <Bot bot={bot} key={index} />
                    )
                  })
                }
              </tbody>
            </table>



            <nav>
              <ul className='pagination'>
                <li className='page-item'>
                  <button onClick={() => {
                    if (!(this.state.currentPage == 1)) {
                      this.setState({ currentPage: this.state.currentPage - 1 });
                      this.slicing();
                    }
                  }} className='page-link'>
                    Previous
                  </button>
                </li>
                {this.Pagination(this.state.botsPerPage, this.state.bots.length)}

                <li className='page-item'>
                  <button onClick={() => {
                    const maxPage = Math.ceil(this.state.bots.length / this.state.botsPerPage);
                    if (!(this.state.currentPage == maxPage)) {
                      this.setState({ currentPage: this.state.currentPage + 1 });
                      this.slicing()
                    }

                  }} className='page-link'>
                    Next
                  </button>
                </li>


              </ul>
            </nav>

            <Link to="/bots/new">
              <button type="button" className="btn btn-success">Create</button>
            </Link>
          </div>
        }
      </div>

    )

  }
}