import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../services/auth';
import { tsPropertySignature } from '@babel/types';



export default function Navbar(props) {
    let userContext = useContext(AuthContext);
    const [search_string, setSearchString] = useState('');

    const search = (

        <form class="form-inline my-2 my-lg-0" action={"/search/" + search_string}>
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={e => setSearchString(e.target.value)} required />
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>


    )

    return (
        <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
            <Link to="/" className="navbar-brand">BorrowShop</Link>
            <div className="collpase navbar-collapse">
                <ul className="navbar-nav mr-auto">
                    <li className="navbar-item">
                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    {userContext.currentUser.isAdmin ?
                        <li className="navbar-item">
                            <Link to="/users" className="nav-link">Users</Link>
                        </li>
                        :
                        <li></li>
                    }

                    <li className="navbar-item">
                        <Link to="/bots" className="nav-link">Bots</Link>
                    </li>
                    <li className="navbar-item">
                        <Link to="/categories" className="nav-link">Categories</Link>
                    </li>
                    <li className="navbar-item">
                        <Link to="/about" className="nav-link">About</Link>
                    </li>
                    {search}
                </ul>

                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                            {userContext.currentUser.signed ?
                                <React.Fragment>
                                    <Link className="nav-link"
                                        to={{ pathname: `/users/${userContext.currentUser.id}`, user: userContext.currentUser }}>{userContext.currentUser.username}</Link>
                                    <button type="button" className="btn btn-dark" onClick={async () => {
                                        await fetchLogOut();
                                        props.history.push('/');
                                        window.location.reload();
                                    }}>
                                        LogOut
                            </button>

                                </React.Fragment>
                                : <React.Fragment>
                                    <button className="btn btn-dark" onClick={() => window.location = "/login"}>LogIn</button>
                                    <button className="btn btn-dark" onClick={() => window.location = "/register"}>Register</button>
                                </React.Fragment>}
                        </div>
                    </li>
                </ul>

            </div>
        </nav>
    );
}


let fetchLogOut = async () => {
    try {
        let res = await fetch('/api/v1/users-log-out', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        });
        let txt = await res.json();
        console.log('Txt: ', txt);
        if (!sessionStorage.getItem('currentUser')) {
            await localStorage.removeItem('currentUser');
        } else {
            await sessionStorage.removeItem('currentUser');
        }
        console.log('All must be deleted');
        return ({ message: 'Logout successful' });

    } catch (error) {
        console.log("Err", error);
        if (!sessionStorage.getItem('currentUser')) {
            await localStorage.removeItem('currentUser');
        } else {
            await sessionStorage.removeItem('currentUser');
        }
        return ({ message: 'Logout failed' });
    }
};