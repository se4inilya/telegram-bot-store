import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { runInThisContext } from 'vm';

export default class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bots: [],
            categories: [],
        }
    }

    componentDidMount() {
        const search = () => {
            axios.get(`/api/v1/bots/search/${this.props.match.params.search_str}`)
                .then(res => { this.setState({ bots: res.data }) })
                .catch(err => this.props.history.push("/error404"))
            axios.get(`/api/v1/categories/search/${this.props.match.params.search_str}`)
                .then(res => { this.setState({ categories: res.data }) })
                .catch(err=> this.props.history.push("/error404"));
        }
        
        search();
    }



    render() {
        return (
            <div>

                <h3>Bots</h3>
                {this.state.bots.length ?
                <ul className='list-group mb-4'>
                    {this.state.bots.map(bot => (
                        <li key={bot.id} className='list-group-item'>
                            <Link to={`/bots/${bot.id}`}>{bot.name}</Link>
                        </li>
                    ))}
                </ul>
                :
                <p>Bots not found</p>
                }

                    <h3>Categories</h3>
                    {this.state.categories.length ?
                    <ul className='list-group mb-4'>
                        {this.state.categories.map(category => (
                            <li key={category.id} className='list-group-item'>
                                <Link to={`/categories/${category.id}`}>{category.name}</Link>
                            </li>
                        ))}
                    </ul>
                    :
                    <p>Categories not found</p>
                }


            </div>
        )
    }
}
