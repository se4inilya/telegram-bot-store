import React, { Component } from 'react';
import axios from 'axios';
import { AuthContext } from '../services/auth';

export default class CreateOfBot extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePrice = this.onChangePrice.bind(this);
        this.onChangeAvatarURL = this.onChangeAvatarURL.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            price: 0,
            avatarURL: '',

        }
    }

    static contextType = AuthContext;

    onChangeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    onChangePrice(e) {
        this.setState({
            price: e.target.value
        })
    }

    onChangeAvatarURL(e) {
        this.setState({
            avatarURL: e.target.files[0]
        })
    }


    onSubmit(e) {
        e.preventDefault();
        let user = this.context;
        const fd = new FormData();

        if (this.state.avatarURL)
            fd.append('image', this.state.avatarURL, this.state.avatarURL.name)

        console.log(user.currentUser.id)
        fd.append('price', this.state.price)
        fd.append('name', this.state.name)
        fd.append('userId', user.currentUser.id);

        const reqOpt = {};
        axios.post('/api/v1/bots/new', fd, reqOpt)
            .then(res => this.props.history.push("/bots/" + res.data.id))
        // .catch(err => this.props.history.push("/error404"));
    }

    render() {
        return (
            <div>
                <h3>Create Bot</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName} />

                    </div>
                    <div className="form-group">
                        <label>Price: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.price}
                            onChange={this.onChangePrice}
                        />
                    </div>
                    <div className="form-group">
                        <label>Image: </label>
                        <input
                            type="file"
                            name="avatarURL"
                            onChange={this.onChangeAvatarURL}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Bot" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
