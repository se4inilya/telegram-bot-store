import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { AuthContext } from '../services/auth';


export default function Bot(props) {
    let userContext = useContext(AuthContext);
    const [bot, setBot] = useState();
    let slug = useParams();
    let bot_id = slug.bot_id;

    useEffect(() => {
        loadBot();
    }, [])

    let loadBot = () => {
        fetch(`/api/v1/bots/${bot_id}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                console.log(response.status)
                if (response.status != 200)
                    props.history.push("/error" + response.status);
                return response.json()

            })
            .then(loaded_bot => {
                console.log(loaded_bot);
                setBot(loaded_bot);
            })
            .catch((error) => {
                props.history.push("/error404");
            })
    }

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const url = "/bots/" + bot_id + "/update";
    return (

        <div>

            {bot &&
                <div>
                    <h3>Bot  {bot.id}</h3>
                    <p><b>Bot name:</b>  {bot.name}
                        <br /><b>Price:</b> {bot.price} <br />
                        <img src={bot.avatarURL} />
                    </p>
                    {userContext.currentUser.id != bot.userId && !userContext.currentUser.isAdmin ?

                        <p></p>
                        :
                        <React.Fragment>
                            <Link to={url}>
                                <button type="button" className="btn btn-primary">Update</button>
                            </Link>
                            <br /><br />
                            <Button variant="danger" onClick={handleShow}>
                                Delete
                    </Button>

                            <Modal show={show} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Removing of bot</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>Do you agree to delete this bot ?</Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>
                                        No
                            </Button>
                                    <Button variant="primary" onClick={() => {
                                        fetch(`/api/v1/bots/${bot_id}`, {
                                            method: 'DELETE',
                                            headers: {
                                                'Accept': 'application/json',
                                                'Content-Type': 'application/json'
                                            }
                                        })
                                            .then(res => {
                                                props.history.push('/bots');
                                            })
                                    }}>
                                        Yes
                            </Button>
                                </Modal.Footer>
                            </Modal>
                        </React.Fragment>

                    }
                </div>
            }




        </div >




    )
}