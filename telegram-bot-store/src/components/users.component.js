import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { AuthContext } from '../services/auth';


export default class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            currentPage: 1,
            usersPerPage: 4
        }

    }
    static contextType = AuthContext;

    componentDidMount() {
        let user = this.context;
        console.log(user);
        if (!user.currentUser.isAdmin) {
            this.props.history.push("/error403");
        }
        const fetchUsers = async () => {
            const res = await axios.get('/api/v1/users');
            this.setState({ users: res.data });
        };

        fetchUsers();
    }

    slicing() {
        const indexOfLastUser = this.state.currentPage * this.state.usersPerPage;
        const indexOfFirstUser = indexOfLastUser - this.state.usersPerPage;
        const currentUsers = this.state.users.slice(indexOfFirstUser, indexOfLastUser);

        return currentUsers;
    }



    Pagination(usersPerPage, totalUsers) {
        const pageNumbers = [];

        for (let i = 1; i <= Math.ceil(totalUsers / usersPerPage); i++) {
            pageNumbers.push(i);
        }

        const changeNumber = (pageNumber) => { this.setState({ currentPage: pageNumber }) }

        return (
            <nav>
                <ul className='pagination'>
                    {pageNumbers.map(number => (
                        number !== this.state.currentPage ?
                            <li key={number} className='page-item'>
                                <button onClick={() => {
                                    changeNumber(number);
                                    this.slicing();
                                }} className='page-link'>
                                    {number}
                                </button>
                            </li>
                            :
                            <li key={number} className='page-item active'>
                                <button onClick={() => {
                                    changeNumber(number);
                                    this.slicing();
                                }} className='page-link'>
                                    {number}
                                </button>
                            </li>
                    ))}
                </ul>
            </nav>
        );
    };

    render() {
        return (
            <div>
                {

                    <div>
                        <h3>Users</h3>
                        <ul className='list-group mb-4'>
                            {this.slicing().map(user => (
                                <li key={user.id} className='list-group-item'>

                                    <Link to={`/users/${user.id}`}>{user.username}</Link>
                                </li>
                            ))}
                        </ul>



                        <nav>
                            <ul className='pagination'>
                                <li className='page-item'>
                                    <button onClick={() => {
                                        if (!(this.state.currentPage == 1)) {
                                            this.setState({ currentPage: this.state.currentPage - 1 });
                                            this.slicing();
                                        }
                                    }} className='page-link'>
                                        Previous
                  </button>
                                </li>
                                {this.Pagination(this.state.usersPerPage, this.state.users.length)}

                                <li className='page-item'>
                                    <button onClick={() => {
                                        const maxPage = Math.ceil(this.state.users.length / this.state.usersPerPage);
                                        if (!(this.state.currentPage == maxPage)) {
                                            this.setState({ currentPage: this.state.currentPage + 1 });
                                            this.slicing()
                                        }

                                    }} className='page-link'>
                                        Next
                  </button>
                                </li>


                            </ul>
                        </nav>
                    </div>
                }
            </div>

        )

    }
}