import React from 'react';

export default function Footer() {
    return (
        <footer class="page-footer font-small black">

            <div class="footer-copyright text-center py-3">© 2019 Telegram Bot Store:
                <a href="/"> telegram-store.herokuapp.com</a>
            </div>

        </footer>
    )
}