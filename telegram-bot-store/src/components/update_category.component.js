import React, { Component } from 'react';
import axios from 'axios';

export default class UpdateCategory extends Component{
    constructor(props){
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            price: 0,
            avatarURL: '',
            
        }
    }

    componentDidMount(){
        axios.get('/api/v1/categories/' + this.props.match.params.category_id)
        .then(res => res.data)
        .then(category => {
            this.setState({
                name : category.name,
            })
        })
        .catch(err => this.props.history.push("/error404"))
    }

    onChangeName(e) {
        console.log("here");
        this.setState({
            name: e.target.value
        })
    }

   
    onSubmit(e) {
        e.preventDefault();
        const bodyOptions = {
            name : this.state.name
        }
            const reqOpt = {};
        axios.put(`/api/v1/categories/${this.props.match.params.category_id}/update`, bodyOptions, reqOpt)
            .then(res => this.props.history.push(`/categories/${this.props.match.params.category_id}`))
            .catch(err => this.props.history.push("/error400"))
    }

    render() {
        return (
            <div>
                <h3>Update Category</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName} />
                            
                    </div>
                    
                    <div className="form-group">
                        <input type="submit" value="Update Category" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
