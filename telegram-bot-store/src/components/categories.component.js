import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default class Categories extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            currentPage: 1,
            categoriesPerPage: 4
        }

    }

    componentDidMount() {
        const fetchCategories = async () => {
            const res = await axios.get('/api/v1/categories');
            this.setState({ categories: res.data });
        };

        fetchCategories();
    }

    slicing() {
        const indexOfLastCategory = this.state.currentPage * this.state.categoriesPerPage;
        const indexOfFirstCategory = indexOfLastCategory - this.state.categoriesPerPage;

        return this.state.categories.slice(indexOfFirstCategory, indexOfLastCategory);
    }



    Pagination(categoriesPerPage, totalCategories) {
        const pageNumbers = [];

        for (let i = 1; i <= Math.ceil(totalCategories / categoriesPerPage); i++) {
            pageNumbers.push(i);
        }

        const changeNumber = (pageNumber) => { this.setState({ currentPage: pageNumber }) }

        return (
            <nav>
                <ul className='pagination'>
                    {pageNumbers.map(number => (
                        number !== this.state.currentPage ?
                            <li key={number} className='page-item'>
                                <button onClick={() => {
                                    changeNumber(number);
                                    this.slicing();
                                }} className='page-link'>
                                    {number}
                                </button>
                            </li>
                            :
                            <li key={number} className='page-item active'>
                                <button onClick={() => {
                                    changeNumber(number);
                                    this.slicing();
                                }} className='page-link'>
                                    {number}
                                </button>
                            </li>
                    ))}
                </ul>
            </nav>
        );
    };

    render() {
        return (
            <div>
                {

                    <div>
                        <h3>Categories</h3>

                        <ul className='list-group mb-4'>
                            {this.slicing().map(category => (
                                <li key={category.id} className='list-group-item'>

                                    <Link to={`/categories/${category.id}`}>{category.name}</Link>
                                </li>
                            ))}
                        </ul>


                        <nav>
                            <ul className='pagination'>
                                <li className='page-item'>
                                    <button onClick={() => {
                                        if (!(this.state.currentPage == 1)) {
                                            this.setState({ currentPage: this.state.currentPage - 1 });
                                            this.slicing();
                                        }
                                    }} className='page-link'>
                                        Previous
                  </button>
                                </li>
                                {this.Pagination(this.state.categoriesPerPage, this.state.categories.length)}

                                <li className='page-item'>
                                    <button onClick={() => {
                                        const maxPage = Math.ceil(this.state.categories.length / this.state.categoriesPerPage);
                                        if (!(this.state.currentPage == maxPage)) {
                                            this.setState({ currentPage: this.state.currentPage + 1 });
                                            this.slicing()
                                        }

                                    }} className='page-link'>
                                        Next
                  </button>
                                </li>


                            </ul>
                        </nav>

                        <Link to="/categories/new">
                            <button type="button" className="btn btn-success">Create</button>
                        </Link>
                    </div>
                }
            </div>

        )

    }
}