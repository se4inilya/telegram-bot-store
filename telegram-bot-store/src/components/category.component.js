import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

export default function Category(props) {

    const [category, setCategory] = useState();
    let slug = useParams();
    let category_id = slug.category_id;

    useEffect(() => {
        loadCategory();
    }, [])

    let loadCategory = () => {
        fetch(`/api/v1/categories/${category_id}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.status != 200)
                    props.history.push("/error" + response.status);
                return response.json()

            })
            .then(loaded_category => {
                console.log(loaded_category);
                setCategory(loaded_category);
            })
            .catch((error) => {
                props.history.push("/error404");
            })
    }

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const url = "/categories/" + category_id + "/update";
    return (

        <div>

            {category &&
                <div>
                    <h3>Category  {category.id}</h3>
                    <p><b>Category name:</b>  {category.name}
                    </p>

                    <Link to={url}>
                        <button type="button" className="btn btn-primary">Update</button>
                    </Link>
                    <br /><br />
                    <Button variant="danger" onClick={handleShow}>
                        Delete
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Removing of category</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>Do you agree to delete this category ?</Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                No
                            </Button>
                            <Button variant="primary" onClick={() => {
                                fetch(`/api/v1/categories/${category_id}/delete`, {
                                    method: 'DELETE',
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    }
                                })
                                    .then(res => {
                                        props.history.push('/categories');
                                    })
                            }}>
                                Yes
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            }




        </div >




    )
}