import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
} from 'react-router-dom'
import { AuthContext } from "../services/auth";
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

export default function User(props) {

    const [user, setUser] = useState();
    let slug = useParams();
    let user_id = slug.user_id;

    useEffect(() => {
        loadUser();
    }, [])

    let userContext = useContext(AuthContext);

    let loadUser = () => {
        if(userContext.currentUser.id != user_id && !userContext.currentUser.isAdmin){
            props.history.push("/error403");
        }
        fetch(`/api/v1/users/${user_id}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                return response.json()

            })
            .then(loaded_user => {
                setUser(loaded_user);
            })
            .catch((error) => {
                props.history.push("/error404");
            })
    }
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const url = "/users/" + user_id + "/update";
    return (

        <div>

            {user &&
                <div>
                    <h3>User  {user.id}</h3>
                    <p><b>User name:</b>  {user.username}
                        <br /><b>E-mail:</b> {user.email} <br />
                        <img src={user.avaUrl} />
                    </p>

                    <Button vatiant="primary" onClick={() => props.history.push(url)}>
                        {/* <button type="button" className="btn btn-primary">Update</button> */}
                        Update
                     </Button>
                    <br /><br />
                    <Button variant="danger" onClick={handleShow}>
                        Delete
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Removing of user</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>Do you agree to delete this user ?</Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                No
                            </Button>
                            <Button variant="primary" onClick={() => {
                                fetch(`/api/v1/users/${user_id}/delete`, {
                                    method: 'DELETE',
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    }
                                })
                                    .then(res => {
                                        props.history.push('/users');
                                    })
                            }}>
                                Yes
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            }




        </div >




    )
}