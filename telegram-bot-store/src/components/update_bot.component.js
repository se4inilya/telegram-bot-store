import React, { Component } from 'react';
import axios from 'axios';
import { AuthContext } from '../services/auth';

export default class UpdateBot extends Component{
    constructor(props){
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePrice = this.onChangePrice.bind(this);
        this.onChangeAvatarURL = this.onChangeAvatarURL.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            price: 0,
            avatarURL: '',
            
        }
    }

    static contextType = AuthContext;

    componentDidMount(){
        let user = this.context;
        axios.get('/api/v1/bots/' + this.props.match.params.bot_id)
        .then(res => res.data)
        .then(bot => {
            user.currentUser.id === bot.userId || user.currentUser.isAdmin ?
            this.setState({
                name : bot.name,
                price : bot.price,
                avatarURL : bot.avatarURL
            })
            :
            this.props.history.push("/error403");
        })
        .catch(err => this.props.history.push("/error404"));
    }

    onChangeName(e) {
        console.log("here");
        this.setState({
            name: e.target.value
        })
    }

    onChangePrice(e) {
        this.setState({
            price: e.target.value
        })
    }

    onChangeAvatarURL(e) {
        this.setState({
            avatarURL: e.target.files[0]
        })
    }

   
    onSubmit(e) {
        e.preventDefault();

        const fd = new FormData();

            if (this.state.avatarURL)
                fd.append('image', this.state.avatarURL, this.state.avatarURL.name)

            fd.append('price', this.state.price)
            fd.append('name', this.state.name)
            console.log(this.state.avatarURL);
            const reqOpt = {};
        axios.put(`/api/v1/bots/${this.props.match.params.bot_id}/update`, fd, reqOpt)
            .then(res => this.props.history.push(`/bots/${this.props.match.params.bot_id}`))
            .catch(err => this.props.history.push("/error400"));
    }

    render() {
        return (
            <div>
                <h3>Update Bot</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName} />
                            
                    </div>
                    <div className="form-group">
                        <label>Price: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.price}
                            onChange={this.onChangePrice}
                        />
                    </div>
                    <div className="form-group">
                        <label>Image: </label>
                        <input
                            type="file"
                            name="avatarURL"
                            onChange={this.onChangeAvatarURL}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Update Bot" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
