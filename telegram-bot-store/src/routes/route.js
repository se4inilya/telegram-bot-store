import React, { useContext } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { AuthContext } from "../services/auth";

export default function RouteWrapper({
    component: Component,
    isPrivate,
    ...rest
}) {
    const userContext = useContext(AuthContext);
    let signed = userContext.currentUser.signed;

    
    console.log("RouterWrapper changed: ", isPrivate, signed);
    if (isPrivate && !signed) {
        return <Redirect to='/login' />;
    }

    return <Route {...rest} component={Component} />;

}

RouteWrapper.propTypes = {
    isPrivate: PropTypes.bool,
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

RouteWrapper.defaultProps = {
    isPrivate: false
};

