import React, { useContext } from 'react';
import { Switch } from 'react-router-dom';
import Route from './route';

import Home from "../components/home.component";
import Users from "../components/users.component"
import Bots from "../components/bots.component";
import Categories from "../components/categories.component";
import About from "../components/about.component";

import Bot from '../components/bot.component';
import CreateOfBot from '../components/create_bot.component';
import UpdateBot from '../components/update_bot.component';

import Category from '../components/category.component';
import CreateOfCategory from '../components/create_category.component';
import UpdateCategory from '../components/update_category.component';

import User from '../components/user.component';
import UpdateUser from '../components/update_user.component';

import Search from '../components/search.component';

import Developer from '../components/developer.component'
import SignIn from '../pages/signIn';
import SignUp from '../pages/signUp';

import Error400 from '../components/error400';
import Error403 from '../components/error403';
import Error404 from '../components/error404';

export default function Routes() {
    
    return (
        <Switch>
            <Route path="/" exact component={Home} />

            <Route path="/search/:search_str" component={Search} isPrivate/>
            <Route path="/bots/new" component={CreateOfBot} isPrivate />
            <Route path="/bots/:bot_id/update" component={UpdateBot} isPrivate />
            <Route path="/bots/:bot_id" component={Bot} isPrivate />
            <Route path="/bots" component={Bots} isPrivate />

            <Route path='/register' component={SignUp} />
            <Route path="/login" component={SignIn} />


            <Route path="/categories/new" component={CreateOfCategory} isPrivate />
            <Route path="/categories/:category_id/update" component={UpdateCategory} isPrivate />
            <Route path="/categories/:category_id" component={Category} isPrivate />
            <Route path="/categories" component={Categories} isPrivate />


            <Route path="/users/:user_id/update" component={UpdateUser} isPrivate />
            <Route path="/users/:user_id" component={User} isPrivate />
            <Route path="/users" component={Users} isPrivate />
            
            <Route path="/developer" component={Developer} />

            <Route path="/about" component={About} />

            <Route path="/error400" component={Error400}/>
            <Route path="/error403" component={Error403}/>
            <Route path="/error404" component={Error404}/>

            <Route component={Error400} />
        </Switch>
    )
}