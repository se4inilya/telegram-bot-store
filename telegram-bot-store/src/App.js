import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


import Navbar from "./components/navbar.component"

import {createBrowserHistory} from 'history';

import {AuthContextProvider} from './services/auth';

import Routes from './routes';

const history = createBrowserHistory();
function App() {
  return (
    <AuthContextProvider>
    <React.Fragment>
    
    <Router history = {history}>
      <div>
      <Navbar history={history}/>
      <div className="container">
        <Routes/>
        </div>
      </div>
    </Router>
    </React.Fragment>
    </AuthContextProvider>
  );
}

export default App;
