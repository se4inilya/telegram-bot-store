const Users = require('../models').users;
const Bots = require('../models').bots;
const Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const utf8 = require('utf8');



module.exports = {
    async create(req, res) {
        try {

            // Validation
            let invalid_email_user = await Users.findOne({
                where: {
                    email: req.body.email
                }
            });
            if (invalid_email_user) {
                console.log('Repeated email');
                return res.status(400).send({ message: 'Account with such email already exists' });
            }
            let invalid_nickname_user = await Users.findOne({
                where: {
                    username: req.body.username
                }
            });
            if (invalid_nickname_user) {
                console.log('Repeated username');
                return res.status(400).send({ message: 'Account with such username already exists' });
            }
            // End of validation
            let user = await Users.create({
                username: req.body.username,
                password_hash: req.body.password,
                email: req.body.email,
                avaUrl: `https://www.gravatar.com/avatar/${md5(utf8.encode(req.body.email.toLowerCase()))}?d=identicon`,
            })

            return res.status(201).send(user); // TODO: Change return to res.render

        }
        catch (error) {
            return res.status(400).send({ message: 'Registration screwed up', error: error });
        }
    },
    async getAll(req, res) {
        try {
            let users = await Users
                .findAll()


            res.status(200).send(users);

        }
        catch (error) { res.status(400).send(error) };
    },
    async getById(req, res) {
        try {
            let user = await Users.findByPk(req.params.id);
            if (!user)
            res.status(404).send("Not Found");
            else
                res.status(200).send(user);
        }

        catch (error) {
            return res.status(400).send(error);
        }
    },
    async setAdmin(req, res) {
        try {
            let user = await Users
                .findByPk(req.params.userId
                );
            if (!user) {
                res.status(404).send("Not Found");
            }
            await user.update({
                role: 1
            });
            return res.status(200).send();
        } catch (error) {
            return res.status(400).send(error);
        }

    },
    async revokeAdmin(req, res) {
        try {
            let user = await Users
                .findByPk(req.params.userId
                );
            if (!user) {
                res.status(404).send("Not Found");
            }
            await user.update({
                role: 0
            });
            return res.status(200).send();
        } catch (error) {
            return res.status(400).send(error);
        }

    },
    async updateById(req, res) {
        try {
            let user = await Users
                .findByPk(req.params.userId, {
                    include: [{
                        model: Bots,
                        as: 'bots',
                    }]
                });
            if (!user) {
                res.status(404).send("Not Found");
            }
            let updated_user = await user.update(req.body, { fields: Object.keys(req.body) });
            return res.status(200).send(updated_user);
        } catch (error) {
            return res.status(400).send({ message: 'Updating user failed', error: error });
        }
    },
    async deleteById(req, res) {
        try {
            let user = await Users.findByPk(
                req.params.id,

            );
            if (!user) {
                res.status(404).send("Not Found");
            }
            await user.destroy();
            res.status(204).send();


        }
        catch (error) {
            res.status(400).send(error);
        }
    },
    async jwt_authenticate(req, res) {
        try {
            const { email, password } = req.body;
            let user = await Users.findOne({ where: { email: email } });
            if (!user) {
                return res.status(401).send({ message: 'Wrong email or password' });
            }
            if (!user.check_password(password)) {
                return res.status(401).send.json({
                    error: 'Incorrect email or password'
                });
            }
            const payload = {
                user: user
            };
            const token = jwt.sign(payload, 'secret');
            res.setHeader('Cache-Control', 'private');
            res.cookie('jwt', token, {
                httpOnly: true, maxAge: 900000, sameSite: true,
                signed: true,
            });
            res.send(user);
        } catch (error) {
            return res.status(500).send({ message: 'Something went wrong', error: error })
        }
    },
    async jwt_logout(req, res) {
        try {

            if (res.clearCookie('jwt')) {
                res.send({message: 'Cookie successfully destroyed'})
            } else {
                res.send({message: 'No jwt cookies set'});
            }
        } catch (error) {
            return res.status(400).send({message: 'Something went wrong, could not destroy cookie', error: error})
        }
    },
}