const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {
    async getAll(req, res) {
        try {
            let bots = await Bots
                .findAll();
            res.status(200).send(bots);
        }
        catch (error) { res.status(400).send(error) };

    },
    getById(req, res) {
        console.log(parseInt(req.params.id, 10))

        return Bots
            .findByPk(req.params.id, {
                include: [{
                    model: Categories,
                    as: 'categories',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                bot => {
                    if (!bot)
                        res.status(404).send("Not Found");
                    else
                        res.status(200).send(bot);
                }
            )
            .catch(error => { res.status(400).send(error) });

    },
    deleteById(req, res) {
        return Bots
            .findOne({
                where: {
                    id: req.params.id,
                }
            })
            .then(bot => {
                if (!bot) {
                    res.status(404).send("Not Found");
                }
                return bot
                    .destroy()
                    .then(() => {

                        res.status(204).send();
                    })
                    .catch(error => res.status(400).send(error));
            }
            )
            .catch(error => res.status(400).send(error));
    },

    async search(req, res) {
        try {
            let bots = await Bots.findAll({
                where: {
                    name: {
                        [Op.iLike]: `%${req.params.search}%`
                    }
                }
            });

            return res.status(200).send(bots);
        } catch (error) {
            return res.status(400).send({ message: 'Searching for bots failed', error: error });
        }
    }
}