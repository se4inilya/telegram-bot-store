const express = require('express');
const users = require('../models').users;
const router = express.Router();
const protectedRouter = express.Router();
let BotsController = require('../controllers/bots');
let CategoriesController = require('../controllers/categories');
let UsersController = require('../controllers/users');
const authHelpers = require('../auth/_helpers');
const cloudinary = require('cloudinary');
const cloudinaryStorage = require("multer-storage-cloudinary");
const multer = require("multer");

const Categories = require('../models').categories;
const Bots = require('../models').bots;
const Users = require('../models').users;

const BotsCategories = require('../models').bots_categories;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

cloudinary.config({
    cloud_name: 'chort1488',
    api_key: '316368948551487',
    api_secret: 'eHJFoqKCm0E5s6jk9Fj6LHZdNr8'
});

const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: "",
    allowedFormats: ["jpg", "png"],
    transformation: [{ width: 500, height: 500, crop: "limit" }]
});
const parser = multer({ storage: storage });


router.post('/users-sign-in', UsersController.jwt_authenticate);

//Bots
router.get('/bots/search/:search', BotsController.search);
router.post('/bots/new', parser.single("image"), (req, res) => {
    let category_id = 1;
    return Bots
        .create({
            name: req.body.name,
            userId: req.body.userId,
            price: req.body.price,
            avatarURL: req.file.url,
        })
        .then(bot => {
            BotsCategories.create({
                category_id: category_id,
                bot_id: bot.id,
            })
                .then(() => {
                    Bots
                        .findByPk(bot.id, {
                            include: [{
                                model: Categories,
                                as: 'categories',
                                required: 'false',
                                attributes: ['id', 'name'],
                                through: { attributes: [] }
                            }]
                        })
                        .then(bot => { res.status(201).send(bot) })
                        .catch(error => res.status(400).send(error));
                })
                .catch(error => res.status(400).send(error));
        })
        .catch(error => {
            res.status(400).send(error);
        })
});
router.get('/bots/:id', BotsController.getById);
router.get('/bots', BotsController.getAll);
router.delete('/bots/:id', BotsController.deleteById);
router.put('/bots/:id/update', parser.single("image"), async (req, res) => {
    try {
        let bot = await Bots
            .findByPk(req.params.id, {
                include: [{
                    model: Categories,
                    as: 'categories',
                }]
            });
        if (!bot) {
            res.render('error', {
                message: 'Not Found',
            });
        }
        console.log(req.body.name);
        let updated_bot = await bot.update({
            name: req.body.name,
            price: req.body.price,
            avatarURL: req.file.url
        });
        return res.status(200).send(updated_bot);
    } catch (error) {
        return res.status(400).send({ message: 'Updating user failed', error: error });
    }
});



//Categories
router.get('/categories/search/:search', CategoriesController.search)
router.post('/categories/new', CategoriesController.create);
router.get('/categories/:id', CategoriesController.getById);
router.get('/categories', CategoriesController.getAll);
router.delete('/categories/:id/delete', CategoriesController.deleteById);
router.put('/categories/:id/update', CategoriesController.updateById);


//Users
router.post('/users/new', UsersController.create);
router.post('/users-log-out', UsersController.jwt_logout);
router.get('/users', UsersController.getAll);
router.get('/users/:id', UsersController.getById);
router.get('/users/:userId/giveadmin', authHelpers.adminRequired, UsersController.setAdmin);
router.get('/users/:userId/revokeadmin', authHelpers.adminRequired, UsersController.revokeAdmin);
router.put('/users/:userId/update', parser.single("image"), async (req, res) => {
    try {
        let user = await Users
            .findByPk(req.params.userId, {
                include: [{
                    model: Bots,
                    as: 'bots',
                }]
            });
        if (!user) {
            res.render('error', {
                message: 'Not Found',
            });
        }
        let updated_user = await user.update({
            username : req.body.username,
            email : req.body.email,
            avaUrl : req.file.url,
        });
        return res.status(200).send(updated_user);
    } catch (error) {
        return res.status(400).send({ message: 'Updating user failed', error: error });
    }
});
router.delete('/users/:id/delete', UsersController.deleteById);

router.get('/me', authHelpers.loginRequired, async (req, res) => {
    let user = await users.findByPk(req.user.id);
    return res.status(200).send(user);
});



module.exports = {
    protected: protectedRouter,
    unprotected: router
}
