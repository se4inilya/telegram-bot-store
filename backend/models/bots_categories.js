'use strict';
module.exports = (sequelize, DataTypes) => {
  const bots_categories = sequelize.define('bots_categories', {
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'categories',
        key: 'id',
        as: 'category_id'
      }
    },
    bot_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'bots',
        key: 'id',
        as: 'bot_id'
      }
    }
  }, {});
  bots_categories.associate = function (models) {
    // associations can be defined here
  };
  return bots_categories;
};