'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING(64),
        unique: true,
        allowNull: false
      },
      password_hash: {
        type: Sequelize.STRING(64),
        allowNull: false
      },
      role: {
        defaultValue :0,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING(64),
        unique: true,
        allowNull: false
      },
      avaUrl: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};