const express = require('express');
const path = require('path');
const hbs = require('express-handlebars');
let logger = require('morgan');
const api = require('./routes/api');
const bodyParser = require('body-parser');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const cors = require('cors');
const flash = require('connect-flash');
const User = require('./models').users;



const app = express();

app.use(cookieParser('secret'));
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'build')));

app.use(session({
    secret: "Some_secret^string",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(cors());
app.use('/api/v1', api.unprotected);
app.use('/api/v1', passport.authenticate('jwt-signin', {
    session: false,
}), (req, res, next) => {
    return next();
}, api.protected);
let authRoute = require('./routes/auth')(app, passport);
require('./auth/passport')(passport, User);

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(process.env.PORT, function () { console.log('Server is ready'); });

// process.env.PORT


